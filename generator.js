"use strict";

// Sample weights
// var sampleWeights = [10, 20, 70];
var sampleWeights = [0, 0, 0];

var auto_use_desc_warning =
  "Some of the descriptions are too long for the Criteria Description field, and can be automatically inserted into the Description column instead.\r\n\r\nPress OK to use Description column, or press Cancel to trim the text and insert into Criteria Description.";

// So we know if we've copied anything yet or if it's still fresh.
var content_copied = false;

// File paths for the resulting zip file.
const DEFLTFILE = "rubrics_d2l.xml";
const MANIFFILE = "imsmanifest.xml";

const ClearRubricConfirm =
  "Are you sure you want to clear the current rubric? This will start fresh, and remove any existing autosaves.";

// Styles for the Word export.
const rowColors = {
  header: "#ddd",
  odd: "white",
  even: "#f0f0f0",
};

const tableStyles = {
  border: "thin solid grey",
  borderCollapse: "collapse",
  fontFamily: "Calibri",
  fontSize: "11pt",
};

const cellStyles = {
  padding: "5px",
  border: "thin solid grey",
  verticalAlign: "top",
};

// TODO: ???
var newCopyDiv;

var userstate_flags = {
  // on for custom points
  // off for text only
  RUBRIC_TYPE_CUSTOM_POINTS: 1,
  // on for finalised
  // off for under development
  RUBRIC_STATE_FINALISED: 2,
  // on for weight
  // off for points
  RUBRIC_POINT_CALC_BY_WEIGHT: 4,
  // on for yes
  // off for no
  USE_DESCRIPTION_MANUAL: 8,
};

function IsUsingDescription() {
  return $('input[name="description"]:checked').val() == 1;
}

function getReapplyButton() {
  return document.querySelector('[data-action="reapply-with-formatting"]');
}

function ShiftColumn(from, to) {
  const cols = ["r1c5", "r1c7", "r1c9", "r1c11", "r1c13"];
  document.querySelectorAll("#rubricTable tbody tr").forEach(function (row) {
    const f = tinymce.get(
      row.querySelector('td[headers="' + cols[from - 1] + '"] .editable').id
    );
    const t = tinymce.get(
      row.querySelector('td[headers="' + cols[to - 1] + '"] .editable').id
    );
    let temp = t.getContent();
    t.setContent(f.getContent());
    f.setContent(temp);
  });
}

function autosave() {
  let xml = getXml(false);
  window.localStorage["autosave"] = xml;
  window.localStorage["autosave_time"] = moment().toISOString();

  updateAutosaveText();
}

function loadAutosave() {
  //if (window.location.search.includes("mmrid")) return;
  let val = window.localStorage["autosave"];
  if (val && val.length > 0) importXml(val, false, true);
}

function checkAutosaveOnLoad() {
  let val = window.localStorage["autosave"];
  if (val && val.length > 0) {
    setTimeout(loadAutosave, 250);
  }
}

function clearAutosave() {
  delete window.localStorage["autosave"];
  delete window.localStorage["autosave_time"];
}

let intervalTimer = null;

function updateAutosaveText() {
  let handle = document.querySelector(".load-autosave");

  function update() {
    let date = window.localStorage.getItem("autosave_time");
    if (date == null) {
      handle.innerText = "Not saved";
    } else {
      handle.innerText = "Last saved " + moment(date).fromNow();
    }
  }

  update();

  if (intervalTimer != null) clearInterval(intervalTimer);
  intervalTimer = setInterval(update, 5000);
}

function configureAutosave() {
  updateAutosaveText();

  $(window).on("keydown", (e) => {
    e = e.originalEvent;
    if ((e.ctrlKey || e.metaKey) && e.key == "s") {
      e.preventDefault();
      autosave();
    }
  });

  // $(document).on('click', '.load-autosave', loadAutosave);
  $(document).on(
    "change",
    '.autosave, input[name="rubricType"], input[name="rubricState"], input[name="rubricCalc"], input[name="description"], #decimalPoints',
    autosave
  );
  $(document).on("click", ".autosave-on-click", autosave);
}

$(function () {
  getReapplyButton().addEventListener("click", onReapply);
  // When the Import File input is changed (has a file provided), run `importZip`
  $("#importFile").on("change", importZip);
  $("#rubricDownload").on("click", exportZip);
  // The output HTML string
  var o = "";

  var tableBody = document.querySelector("#rubricTable tbody");
  // Prefills with a table from some sample weights defined above.
  for (var i = 0; i < sampleWeights.length; i++) {
    manipulateRow(0, "Add", false);
  }
  $("#rubricTable button").addClass("btn");

  // Adds a column for each `th` as a styling tool.
  //	This should probably be deprecated, as colgroups and cols are obselete as of HTML5.
  // $('#rubricTable th').each(function (i, e) { $('#rubricTable colgroup').append(i > 3 && i % 2 == 0 ? '<col data-points="true">':'<col data-points="false">') });

  // Moves the Rubric Weighting selector into the newly generated table.
  /** $("#BMTselector").appendTo("#rubricTable tfoot tr td:first").show(); */
  // Whenever the weighting selector changes, run the handler function.
  /** $("#BMTselector select").change(adjustBMT); */
  // adjustBMT();

  var detailsRadios = $("#rubricMaker tfoot fieldset:eq(0) input[type=radio]");
  detailsRadios.parent().hide();
  detailsRadios.eq(2).prop("checked", true).parent().show();
  $('input[name=rubricCalc], input[name="description"]', "#rubricMaker").on(
    "change",
    switchCalcAndAdjustBMT
  );
  $(".recalc, #decimalPoints", "#rubricMaker").on("change", recalculate);
  $(document).on("change", ".recalc", recalculate);
  $(document).on("click", "#rubricMaker tbody button", manipulateRow);
  $(document).on("click", "#rubricClear", clearRubric);
  $('#rubricMaker input[name="rubricType"]').on(
    "click",
    switchCalcAndAdjustBMT
  );
  // switchCalcAndAdjustBMT();
  initTinyMCE();
  $("#rubricTitle").focus();
  var copyRubric = new ClipboardJS("#rubricCopy", {
    target: function (t) {
      var newTable = document.createElement("table");
      newTable.setAttribute(
        "style",
        "border:1px solid #000;border-collapse: collapse;"
      );
      var newRow = document.createElement("tr");
      var sourceObj = $("#rubricTable th");
      for (var i = 0; i < sourceObj.length; i += 2) {
        if ($(sourceObj[i]).is(":visible")) {
          var newCol = document.createElement("th");
          newCol.setAttribute("style", "border:1px solid #000");
          newCol.innerHTML = sourceObj[i].innerHTML;
          newRow.appendChild(newCol);
        }
      }
      newTable.appendChild(newRow);

      var sourceObj = $("#rubricTable tbody tr");
      for (var i = 0; i < sourceObj.length; i++) {
        newRow = document.createElement("tr");
        var sourceCols = $("td", sourceObj[i]);
        for (var j = 0; j < sourceCols.length; j += 2) {
          var thisCell = $(sourceCols[j]);
          if (thisCell.is(":visible")) {
            var newCol = document.createElement("td");
            newCol.setAttribute("style", "border:1px solid #000");
            if (j == 0) {
              var marx = $("input", sourceCols[j + 1]).val(); // Karl or Groucho?
              var criteria = $("textarea", thisCell).val();
              if (criteria.indexOf(marx) < 0) {
                criteria += $("#rubricGrade").is(":visible")
                  ? " Weight: " + marx + "%"
                  : " Points: " + marx;
              }
              newCol.innerHTML = "<p>" + criteria + "</p>";
            } else {
              newCol.innerHTML = $("div", thisCell).html();
            }
            newRow.appendChild(newCol);
          }
        }
        newTable.appendChild(newRow);
      }
      newCopyDiv = document.createElement("div");
      newCopyDiv.innerHTML =
        "<h1>" + $("#rubricTitle").val() + "</h1>" + $("#rubricDesc").html();
      newCopyDiv.appendChild(newTable);
      newCopyDiv.id = "copyTable";
      document.body.appendChild(newCopyDiv);
      return newCopyDiv;
    },
  });
  copyRubric.on("success", function (e) {
    document.body.removeChild(newCopyDiv);
    alert(
      "This rubric has been copied to the clipboard. You can now paste the rubric into a new Word document."
    );
  });

  $('input[name="rubricType"]').on("change", save_flags);
  $('input[name="rubricState"]').on("change", save_flags);
  $('input[name="rubricCalc"]').on("change", save_flags);
  $('input[name="description"]').on("change", save_flags);

  $(".autopopulate a").on("click", populate_criteria);
  $(".swap-cols").on("click", function (e) {
    e.preventDefault();
    $("#rubricTable tbody tr").each(function (index, row) {
      var desc = $(".description", row).text();
      var cdesc = $(".criterion", row).val();

      $(".description", row).text(cdesc);
      $(".criterion", row).val(desc);

      $(".description", row).trigger("change");
    });
  });

  $("button.left[data-index]").on("click", function (e) {
    ShiftColumn(
      $(this).attr("data-index"),
      parseInt($(this).attr("data-index")) - 1
    );
  });
  $("button.right[data-index]").on("click", function (e) {
    ShiftColumn(
      $(this).attr("data-index"),
      parseInt($(this).attr("data-index")) + 1
    );
  });

  load_flags();
  // recalculate();
  switchCalcAndAdjustBMT();

  configureAutosave();

  if (post_data.rubric) {
    const zip = JSON.parse(post_data.rubric);
    setTimeout(() => importZip({ target: { files: [zip] } }), 500);
  } else checkAutosaveOnLoad();

  $("#rubricDownloadAsWord").on("click", saveToWordDocument);
});

function switchCalcAndAdjustBMT() {
  switchCalculation();
  /** setTimeout(adjustBMT, 250); */
  // adjustBMT();
}

/**
 * function adjustBMT() {
 *   var self = $("#BMTselector select");
 *
 *   $("#rubricTable tfoot td:visible").each(function (i, e) {
 *     if (i > 0 && i % 2 == 0)
 *       $("input[type=radio]", e)
 *         .eq(
 *           $(self)
 *             .val()
 *             .substr(i / 2 - 1, 1)
 *         )
 *         .prop("checked", true);
 *     recalculate();
 *   });
 * }
 */

function clearRubric() {
  if (confirm(ClearRubricConfirm)) {
    clearAutosave();
    const url = new URL(window.location.href);
    // if (url.searchParams.has("mmrid")) {
    //   url.searchParams.delete("mmrid");
    // }
    window.location = url.href;
  }
}

function populate_criteria(e) {
  e.preventDefault();
  let existingContentLength = Array.from(
    document.querySelectorAll("textarea.criterion")
  )
    .map((ta) => ta.value.trim().length)
    .reduce((o, v) => o + v, 0);
  if (
    existingContentLength > 0 &&
    !confirm(
      "There is already content in the criterion description. Do you wish to override it?"
    )
  )
    return;

  document
    .querySelectorAll("textarea.criterion")
    .forEach(function (textarea, index) {
      textarea.value = "Criterion " + (index + 1);
    });
}

function initTinyMCE() {
  tinymce.init({
    selector: "div.editable",
    paste_data_images: true,
    theme: "silver",
    toolbar: false,
    menubar: false,
    statusbar: false,
    inline: true,
    plugins: [
      "link",
      "autolink",
      "paste",
      "table",
      "advlist",
      "lists",
      "advlist",
      "autolink",
    ],
    contextmenu:
      "link numlist bulllist inserttable | cell row column deletetable",
    insert_toolbar: "",
    //selection_toolbar:
    //"bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat",
    setup: function (ed) {
      ed.on("change", autosave);
    },
    init_instance_callback: function (editor) {
      editor.on("PastePostProcess", function (e) {
        // Only process paste on the Bulk Paste tool
        if (e.target.id != "bulkPaste") return;

        var rowCount = $("tr", e.node).length;
        if (rowCount <= 0) {
          alert("No table rows found. Can't bulk paste.");
          e.node.innerHTML = "";
          return;
        } else {
          var appendAdjustment = 0;
          var rows = [];
          $("tr", e.node).each(function (i, el) {
            // Loop through each row copied
            if ($(el).parents("tr").length == 0) {
              if (el.innerText.trim().length > 0) rows.push(el);
            }
          });
          rowCount = rows.length;
          if ($('input[name="pasteType"]:checked').val() == 0) {
            // replace
            $("#rubricTable tbody tr div.editable").html(""); // Clear any old content
            adjustRows(rowCount);
          } else {
            // append
            appendAdjustment = $("#rubricTable tbody tr").length;
            adjustRows(rowCount + appendAdjustment);
          }
          /**
           * var auto_use_desc = false;
           * for (var i = 0; i < rows.length; i++) {
           *   if (rows[i].cells[0].innerText.trim().length > 255) {
           *     auto_use_desc = true;
           *     break;
           *   }
           * }
           */
          var use_desc = true;
          var has_criterion = false;
          /** var use_desc = auto_use_desc && confirm(auto_use_desc_warning); */
          document.querySelector(
            `input[name="description"][value="${use_desc ? "1" : "0"}"]`
          ).checked = true;
          // if (use_desc) {
          // }
          let data = [];
          $(rows).each(function (i, el) {
            if ($(el).parents("tr").length == 0) {
              var elems = $(el).children("td");
              let d = [];
              for (var j = elems.length - 1; j >= 0; j--) {
                var colspans = parseInt($(elems[j]).attr("colspan")) || 1;
                if (colspans > 1) {
                  for (var k = 0; k < colspans - 1; k++) {
                    d.push("-");
                  }
                }
                d.push($(elems[j]).html());
              }

              const info = {
                Criteria: d[5] || "<p>cont.</p>",
                Description: "",
                HD: fixPastedHTML(d[4]),
                DN: fixPastedHTML(d[3]),
                CR: fixPastedHTML(d[2]),
                PP: fixPastedHTML(d[1]),
                NN: fixPastedHTML(d[0]),
              };

              if (d.length == 7) {
                has_criterion = true;
                info.Description = info.Criteria;
                info.Criteria = d[6];
              }

              data.push(info);
            }
          });
          var rows = document.querySelectorAll("#rubricTable tbody tr");
          data.forEach(function (data, index) {
            // Shorthand functions to update the tinymce editors
            function updateCell(row, tag, content) {
              tinymce
                .get(
                  row
                    .querySelector('[data-for="' + tag + '"]')
                    .getAttribute("id")
                )
                .setContent(content);
            }
            // Shorthand function to update the criterion textarea
            function updateCriterion(row, content) {
              row.querySelector('[data-for="criterion"]').innerHTML = content;
            }
            // Get the current row to modify
            var row = rows[index + appendAdjustment];
            // Get the criteria text
            var criterion = data.Criteria;
            // Strip the HTML out, and clean it up.
            criterion = $(criterion, "<span>")
              .text()
              .replace(/[•·]/g, "")
              .replace(/\s/gm, " ")
              .replace(/\s\s+/g, " ");
            // If we want to use the Description column, drop it straight into there.
            //	Otherwise, trim it down and drop it into the Criterion Description textarea.
            if (!has_criterion) {
              if (use_desc) {
                updateCriterion(row, `Criterion ${index + 1}`);
                updateCell(row, "description", criterion);
              } else {
                updateCriterion(row, criterion.trim().substring(0, 254).trim());
                updateCell(row, "description", "");
              }
            } else {
              updateCriterion(row, criterion.trim().substring(0, 254).trim());
              updateCell(row, "description", data.Description);
            }

            // Add the criteria values.
            updateCell(row, "hd", data.HD);
            updateCell(row, "dn", data.DN);
            updateCell(row, "cr", data.CR);
            updateCell(row, "pp", data.PP);
            updateCell(row, "nn", data.NN);

            var info = criterion + " " + data.Description;

            if (info != "") {
              var ar = info.match(/\d[.\d]*%/gm); // match any percentage
              if (ar != null && ar.length > 0)
                document.getElementById("rubricCalcWeight").checked = true;
              // percentage found, so make sure to switch to percentage mode
              else {
                ar = info.match(
                  /(\d[\d.]* mark)|(mark(s)??(:)??\s*\d[\d.]*)|(\d[\d.]* point(s)?)|(point(s)??(:)??\s*\d[\d.]*)/gim
                ); // match any number followed by the string ' mark'
                if (ar != null && ar.length > 0)
                  document.getElementById("rubricCalcPoints").checked = true; // marks found, so make sure to switch to points mode
              }
              if (ar != null && ar.length > 0) {
                // if valid number found, pop it in the weight
                row.querySelector(".weights").value = parseFloat(
                  ar[ar.length - 1].match(/\d[\d.]*/gim)[0]
                );
              }
            }
          });
          // $(rows).each(function (i, el) {
          // 	// Loop through each row copied, examine each cell,
          // 	if ($(el).parents("tr").length == 0) {
          // 		var elems = $(el).children("td");
          // 		var descCount = $( "#rubricTable tbody tr:eq(" + (i + appendAdjustment) + ") div.mce-content-body" ).length;
          // 		let data = [];
          // 		for (var j = elems.length - 1; j >= 0; j--) {
          // 			if(j > 0) {
          // 				var colspans = parseInt($(elems[j]).attr('colspan')) || 1;
          // 				if(colspans > 1) { for(let k=0; k<colspans-1;k++) { data.push('-'); } }
          // 				data.push(fixPastedHTML($(elems[j]).html()))
          // 			} else data.push(elems[j]);
          // 		}
          // 		for (var j = data.length - 1; j >= 0; j--) {
          // 			// Loop through each column copied in reverse, and place in appropriate column
          // 			if (j > 0)
          // 			{
          // 				tinymce
          // 				.get( $( "#rubricTable tbody tr:eq(" + (i + appendAdjustment) + ") div.mce-content-body:eq(" + (descCount - (data.length - j)) + ")" ).attr("id") )
          // 				.setContent(data[j]);
          // 			} else {
          // 				var $elem = $(data[j]);
          // 				$elem.html($elem.html().replace(/<p>/g, "<p> "));
          // 				// Remove bullet points, replace multiple whitespace with single space
          // 				var proposedCriteriaDesc = $elem
          // 					.text()
          // 					.replace(/[•·]/g, "")
          // 					.replace(/\s/gm, " ")
          // 					.replace(/\s\s+/g, " ");
          // 				if (use_desc) {
          // 					tinymce
          // 						.get(
          // 							$(
          // 								"#rubricTable tbody tr:eq(" +
          // 								(i + appendAdjustment) +
          // 								") div.mce-content-body:eq(0)"
          // 							).attr("id")
          // 						)
          // 						.setContent(proposedCriteriaDesc.trim());
          // 				} else {
          // 					$(
          // 						"#rubricTable tbody tr:eq(" +
          // 						(i + appendAdjustment) +
          // 						") textarea:first"
          // 					).val(proposedCriteriaDesc.substring(0, 254).trim());
          // 				}
          // 				if (proposedCriteriaDesc != "") {
          // 					var ar = proposedCriteriaDesc.match(/\d[.\d]*%/gm); // match any percentage
          // 					if (ar != null && ar.length > 0)
          // 						document.getElementById(
          // 							"rubricCalcWeight"
          // 						).checked = true;
          // 					// percentage found, so make sure to switch to percentage mode
          // 					else {
          // 						ar = proposedCriteriaDesc.match(
          // 							/(\d[\d.]* mark)|(mark(s)??(:)??\s*\d[\d.]*)/gim
          // 						); // match any number followed by the string ' mark'
          // 						if (ar != null && ar.length > 0)
          // 							document.getElementById(
          // 								"rubricCalcPoints"
          // 							).checked = true; // marks found, so make sure to switch to points mode
          // 					}
          // 					if (ar != null && ar.length > 0)
          // 						$(
          // 							"#rubricTable tbody tr:eq(" +
          // 							(i + appendAdjustment) +
          // 							") .weights"
          // 						).val(
          // 							parseFloat(ar[ar.length - 1].match(/\d[\d.]*/gim)[0])
          // 						); // if valid number found, pop it in the weight
          // 				}
          // 			}
          // 		}
          // 	}
          // });
          switchCalculation();
        }
        e.node.innerHTML = "";
        content_copied = true;
        // autosave();
        return;
      });
    },
  });
}

function fixPastedHTML(str) {
  var $str = document.createElement("div");
  // We have bullet points, so we need to process them.
  // First, we'll put our value within a 'div' element, so that we can navigate it's DOM.
  $str.innerHTML = str;

  // If there's a '<td>' element, Then get the html inside the cell.
  $str
    .querySelectorAll("table,tr,th,td")
    .forEach((node) => node.replaceWith(...node.children));

  $str.querySelectorAll("p,span").forEach((node) => {
    if (node.innerText.trim().length == 0) node.remove();
    if (node.innerText.match(/[•·]\s+/g) != null) {
      let li = document.createElement("li");
      node.insertAdjacentElement("beforebegin", li);
      li.appendChild(node);
      node.innerText = node.innerText.replace(/[•·]\s+/g, "");
    }
  });
  // if ($str.querty("<td>") !== -1) {
  // 	str = $("td", $(str)).html();
  // }

  // Strip empty paragraphs, including ones just filled with whitespace or whitespace rendering characters.
  // str = str.replace(/<p>\s*(&nbsp;)*\s*(<br>)*\s*<\/p>/g, "");
  // Convert &nbsp; to an empty space.
  // str = str.replace(/&nbsp;/g, " ");

  // If there's no bullet points, return the result.
  // if (str.match(/[•·]\s+/g) == null) return str;

  // Lets set the original `str` object back to the HTML we've just modified.
  str = $str.innerHTML;

  // Find the index of the last closing </li> tag.
  // var lastLI = str.lastIndexOf("</li>");

  // str = str.substring(0, lastLI) + "</li></ul>" + str.substring(lastLI + 5);
  // return str.replace(/<li>/, "<ul><li>").replace(/[•·]\s+/g, "");

  return str;
}

function clearAllFormatting() {
  $("#rubricTable .editable").each(function (i, e) {
    tinymce.get($(e).attr("id")).setContent($(e).text().replace(/\./g, ". "));
  });
}

function adjustRows(rowCount) {
  while (rowCount >= $("#rubricTable tbody tr").length) {
    // Add rows if not enough in table
    manipulateRow(0, "Add", $("#rubricTable tbody tr:last-child"));
  }
  while ($("#rubricTable tbody tr").length > rowCount) {
    // Remove rows if too many in table
    manipulateRow(0, "Remove", $("#rubricTable tbody tr:last-child"));
  }
}

function manipulateRow(e, rowChangeType, row) {
  if (rowChangeType == undefined) rowChangeType = $(this).text().trim();
  if (row == undefined) row = $(this).closest("tr");

  switch (rowChangeType) {
    case "Add":
      var row = document.importNode(
        document.querySelector("template#criterion-row").content,
        true
      );
      row
        .querySelectorAll(".editable")
        .forEach((editor, i) => (editor.id = "mce_" + Date.now() + "" + i));
      document.querySelector("#rubricTable tbody").appendChild(row);
      initTinyMCE();
      //$(".recalc", row).change(recalculate);
      break;
    case "Remove": {
      if (e == 0) row.remove();
      else {
        let len = 0;
        $(".editable", row).each(function (i, e) {
          len += $(e).text().trim().length;
        });
        if (len == 0) row.remove();
        else if (
          len > 0 &&
          confirm("Are you sure you want to remove this row?")
        )
          row.remove();
      }
      break;
    }
    case "↑":
      row.insertBefore(row.prev());
      break;
    case "↓":
      row.insertAfter(row.next());
      break;
  }
  // $("#rubricTable tbody button")
  // 	.off("click")
  // 	.on("click", manipulateRow);
  $(".removeRow,.moveRowUp,.moveRowDown", "#rubricTable").prop(
    "disabled",
    false
  );
  if ($("#rubricTable tbody tr").length < 2)
    $("#rubricTable .removeRow:first").prop("disabled", true);
  $("#rubricTable .moveRowUp:first").prop("disabled", true);
  $("#rubricTable .moveRowDown:last").prop("disabled", true);
  $("#rubricTable tbody tr").each(function (i, e) {
    $("textarea", e).attr("placeholder", "Enter Criterion (up to 255 chars)");
  });
  recalculate();
  $("#rubricTable div.editable").trigger("change");
  // autosave();
}

function switchCalculation() {
  var isText = $("#rubricTypeText").prop("checked");
  $("#rubricTable tr td,#rubricTable tr th").show();
  // $("#rubricMaker .showRubricGrade").toggle(!isText && isWeight);
  $("#rubricMaker > fieldset#rubricInformation > fieldset:last").toggle(
    !isText
  );
  $("#rubricMaker .recalc").toggle(!isText);
  $("#rubricTable tfoot").toggle(!isText);
  $("#rubricTable").attr("data-text-only", isText);

  var isWeight = $("#rubricCalcWeight").prop("checked");
  if (isText) $("#r1c2").html("Criteria Adjust");
  else
    $("#r1c2").html(isWeight ? "Criteria<br>Weight %" : "Criteria<br>Points");
  $(".notice.check-score").toggle(!isText);

  $("#rubricMaker tfoot td:first span").text(
    isWeight ? "Overall Weight %" : "Overall Points"
  );
  $("#rubricTable").attr("data-is-weight", isWeight);
  if (!$('input[name="description"][value="0"]').is(":checked")) {
    $("#rubricMaker").attr("data-description", "show");
  } else {
    $("#rubricMaker").attr("data-description", "hide");
    /** $("input[name=bmt1]:eq(2)").attr("checked", "checked"); */
  }

  // $("#rubricTable tr").each(function (ind, el) {
  // 	$("td,th", el).each(function (i, e) {
  // 		if (i >= 3 && (i + 1) % 2 == 0) {
  // 			if ($(e).is(":visible")) {
  // 				$(e).toggle(!isText);
  // 			}
  // 		}
  // 	});
  // });

  if (!isText && isWeight) {
    $(".showRubricGrade").slideDown();
  } else {
    $(".showRubricGrade").slideUp();
  }

  if (isText) return;
  recalculate();
}

function recalculate() {
  var isWeight = $("#rubricCalcWeight").prop("checked");
  var sum = 0;
  $("#rubricMaker .weights").each(function () {
    sum += +this.value;
    $(this).attr("data-val", this.value);
    this.parentNode.parentNode
      .querySelectorAll(".points-col")
      .forEach((n) =>
        n.previousElementSibling.setAttribute("data-total", this.value)
      );
  }); // sum all weights/points
  $("#overallTotal").removeClass("error").val(sum);
  if (isWeight && sum != 100) $("#overallTotal").addClass("error"); // highlight overwall weight if it doesn't add up to 100%
  var tot = isWeight ? $("#rubricGrade").val() : $("#overallTotal").val(); // get maximum total as will be reported by MyLO

  $("#total0").val(tot * 1.0 + 1); // Descriptions
  $("#total0")
    .parent()
    .prev()
    .attr("data-val", (tot * 1.0 + 1).toFixed(2)); // Descriptions

  $("#total1").val(tot * 0.8); // HD
  $("#total1")
    .parent()
    .prev()
    .attr("data-val", (tot * 0.8).toFixed(2)); // HD

  $("#total2").val(tot * 0.7); // DN
  $("#total2")
    .parent()
    .prev()
    .attr("data-val", (tot * 0.7).toFixed(2)); // DN

  $("#total3").val(tot * 0.6); // CR
  $("#total3")
    .parent()
    .prev()
    .attr("data-val", (tot * 0.6).toFixed(2)); // CR

  $("#total4").val(tot * 0.5); // PP
  $("#total4")
    .parent()
    .attr("data-val", (tot * 0.5).toFixed(2)); // PP

  $("#total5").val(0); // NN
  $("#total5").parent().prev().attr("data-val", 0); // NN

  $("#rubricMaker tfoot .calctotal").each(function (i, e) {
    // Calculate totals
    $(this).val(getPoints(i, tot));
  });

  $("#rubricTable tbody tr").each(function (index, row) {
    // Calculate points for each row
    $(".points", row).each(function (i, e) {
      var val = localFloor(
        ($(".weights", row).val() *
          $("#rubricMaker tfoot fieldset:eq(" + i + ") .calctotal").val()) /
          $("#overallTotal").val()
      );
      val = isNaN(val) ? 0 : val;
      $(this).parent().prev().attr("data-val", val);
      $(this).val(val);
    });
  });
}

function localFloor(number) {
  var roundLevel = $("#decimalPoints option:selected").val();
  var roundAmount = roundLevel == 0.5 ? 2 : Math.pow(10, roundLevel);
  return Math.floor(number * roundAmount) / roundAmount;
}

function getPoints(columnIndex, points) {
  const firstColumn = IsUsingDescription() ? 0 : 1;

  var bmtIndex = columnIndex == firstColumn ? 2 : 1;
  var thisPoints = 0;
  var topRound = $("#decimalPoints option:selected").val();
  topRound = topRound == 0.5 ? 0.5 : Math.pow(10, -topRound);
  switch (columnIndex) {
    case 0: // details
    case 1: // HD
      switch (bmtIndex) {
        case 0:
          thisPoints = points * 0.8;
          break;
        case 1:
          thisPoints = points * 0.9;
          break;
        default:
          thisPoints = points;
      }
      break;
    case 2: // DN
      switch (bmtIndex) {
        case 0:
          thisPoints = points * 0.7;
          break;
        case 1:
          thisPoints = points * 0.75;
          break;
        default:
          thisPoints = points * 0.8 - topRound;
      }
      break;
    case 3: // CR
      switch (bmtIndex) {
        case 0:
          thisPoints = points * 0.6;
          break;
        case 1:
          thisPoints = points * 0.65;
          break;
        default:
          thisPoints = points * 0.7 - topRound;
      }
      break;
    case 4: // PP
      switch (bmtIndex) {
        case 0:
          thisPoints = points * 0.5;
          break;
        case 1:
          thisPoints = points * 0.55;
          break;
        default:
          thisPoints = points * 0.6 - topRound;
      }
      break;
    case 5: // NN
      switch (bmtIndex) {
        case 0:
          thisPoints = 0;
          break;
        case 1:
          thisPoints = points * 0.25;
          break;
        default:
          thisPoints = points * 0.5 - topRound;
      }
      break;
  }
  thisPoints = Math.round(thisPoints * 100) / 100;
  return thisPoints;
}

function getXml(checkForErrors) {
  var bFailed = false;
  var CriterionXML = "<criteria>";

  var LEVELID = 123456;
  var isText = $("#rubricTypeText").prop("checked");
  var rTitle = $("#rubricTitle").val().trim();
  var criteriaLevels = [];
  var cols = $("#rubricTable thead th:visible");
  var Ind = 0;
  for (var i = 2; i < cols.length; i += 2) {
    // Step 1: Loop through and store the criteria group level set
    criteriaLevels[criteriaLevels.length] = {
      Name: cols.eq(i).prop("title"),
      Tag: cols.eq(i).attr("data-for"),
      Order: Ind + 1,
      ID: LEVELID + Ind,
      Score: isText
        ? 0
        : $("#rubricTable tfoot td:visible input.valtotal")
            .eq(i / 2 - 1)
            .val(),
    };
    Ind += 1;
  }

  // Step 2: Loop through and store the criteria descriptors and values
  //	Note, this loops each row.
  $("#rubricTable > tbody > tr").each(function (i, e) {
    if (!bFailed) {
      var cols = $(e).find("> td:visible");
      var criteriaDesc = $("textarea", cols.eq(0)).val().trim();
      if (criteriaDesc == "" && checkForErrors) {
        alert("Each criterion must have a description.");
        $("textarea", cols.eq(0)).focus();
        bFailed = true;
        return;
      }
      CriterionXML +=
        '<criterion name="' +
        htmlDecode(criteriaDesc.replace(/&nbsp;/g, " ").replace(/&/g, "and"))
          .replace(/\n/g, "&#xA;")
          .substr(0, 255) +
        '" sort_order="' +
        (i + 1) +
        '"><cells>';

      var broken = false;
      $(".editable:visible", e).each(function (i, e) {
        if (broken) return;
        if ($(e).text().trim() == "" && checkForErrors) {
          alert(
            "Oops, there seems to be a problem with one of the content cells. Make sure no cells are empty, and try again."
          );
          broken = true;
          $(e).focus();
          bFailed = true;
          return;
        }

        let data = $(e)
          .html()
          .replace(/[\u2018\u2019]/g, "'") // Replace 'smart' single quotes with standard ones
          .replace(/[\u201C\u201D]/g, '"'); // Replace 'smart' double quotes with standard ones

        let target = $(e).attr("data-for");

        if (target == "description") {
          data =
            '<div class="description" style="background-color: #dcdcdc;font-weight: bold;width: fit-content;padding: 15px;">' +
            data +
            "</div>";
        }

        let obj = criteriaLevels.find((o) => o.Tag == target);
        let value = isText ? "" : $(e).parent().attr("data-val");
        // let value = isText ? "" : $("input", e).val();

        let str =
          '<cell level_id="' +
          obj.ID +
          '" cell_value="' +
          value +
          '"> <description text_type="text/html"><html><![CDATA[' +
          data +
          ']]></html> </description> <feedback text_type="text/html"> <text /> </feedback> </cell>';
        CriterionXML += str;
      });
      CriterionXML += "</cells></criterion>";
    }
  });
  if (bFailed) return;
  CriterionXML = CriterionXML + "</criteria>";
  // scoring_method="3" is Custom Points, "0" is text only; state="2" is Under Development, "0" is finalised
  var outXML =
    '<rubrics schemaversion="v2011">' +
    '<rubric id="1" name="' +
    rTitle.replace("&", "and") +
    '" type="1" scoring_method="' +
    (isText ? "0" : "3") +
    '" display_levels_in_des_order="True" state="' +
    $('input[name="rubricState"]:checked').val() +
    '" score_visible_to_assessed_users="False" usage_restrictions="Competency">' +
    '<description text_type="text/html"><html><![CDATA[' +
    $("#rubricDesc").html() +
    "]]></html></description>" +
    '<criteria_groups><criteria_group name="Criteria" sort_order="1"><level_set><levels>'; // Start output XML with rubric title and description
  criteriaLevels.forEach(function (element) {
    outXML +=
      '<level name="' +
      element.Name +
      '" sort_order="' +
      element.Order +
      '" level_id="' +
      element.ID +
      '" />'; // Add each of the levels (HD, DN, etc)
  });
  outXML +=
    "</levels></level_set>" +
    CriterionXML +
    "</criteria_group></criteria_groups><overall_level_set><overall_levels>"; // Append criterion XML
  for (var i = criteriaLevels.length - 1; i >= 0; i -= 1) {
    // Add overall level section
    let name = criteriaLevels[i].Name;
    if (name == "Description") name = "N/A";

    outXML +=
      '<overall_level name="' +
      name +
      '" sort_order="' +
      criteriaLevels[i].Order +
      (isText ? "" : '" range_start_value="' + criteriaLevels[i].Score) +
      '">' +
      '<description text_type="text"><text /></description><feedback text_type="text"><text /></feedback></overall_level>';
  }
  outXML += "</overall_levels></overall_level_set></rubric></rubrics>";
  return outXML;
}

function exportZip() {
  let text_only = document.querySelector("#rubricTypeText").checked;
  /**
   * var bad_weights = document.querySelectorAll(
   *   'input.weights.recalc[data-val="0"]'
   * ).length;
   * if (bad_weights > 0 && !text_only) {
   *   alert(
   *     "There are " +
   *       bad_weights +
   *       " criterion rows that have a weight of 0. Please correct this before trying to download the rubric."
   *   );
   *   return;
   * }
   */

  var rTitle = $("#rubricTitle").val().trim();
  if (rTitle == "")
    return errorOut(
      "Please enter a title for your new Rubric.",
      "#rubricTitle"
    );

  let outXML = getXml(true);
  if (outXML) {
    var zip = new JSZip(); // Create new Zip file
    zip.file(DEFLTFILE, outXML); // add rubric XML to zip, and then manifest
    zip.file(
      MANIFFILE,
      '<?xml version="1.0" encoding="UTF-8"?><manifest identifier="D2L_61589" xmlns:d2l_2p0="http://desire2learn.com/xsd/d2lcp_v2p0" xmlns:scorm_1p2="http://www.adlnet.org/xsd/adlcp_rootv1p2" xmlns="http://www.imsglobal.org/xsd/imscp_v1p1"><resources><resource identifier="res_rubrics" type="webcontent" d2l_2p0:material_type="d2lrubrics" d2l_2p0:link_target="" href="rubrics_d2l.xml" title="" /></resources></manifest>'
    );
    zip.generateAsync({ type: "blob" }).then(function (blob) {
      saveAs(blob, rTitle + ".zip");
    });

    clearAutosave();
  }
}

let lastContent = "";
function onReapply() {
  if (
    confirm(
      "This will override all content and re-import. Are you sure you wish to continue?"
    )
  ) {
    importXml(lastContent, false);
  }
}

function importXml(content, withFormatting = true, byAutosave = false) {
  var xmlDoc = $.parseXML(content);
  if ($("rubric", xmlDoc).length != 1) {
    alert(
      $("rubric", xmlDoc).length +
        " rubrics found in this zip file. This Rubric Maker only supports zip files with one rubric in it."
    );
    return;
  }

  lastContent = content;
  var criteria = $("criteria criterion", xmlDoc);
  $("#rubricTitle").val($("rubric", xmlDoc).attr("name")); // retrieve rubric name
  var isText = $("rubric", xmlDoc).attr("scoring_method") == 0;
  var descVal = "";
  if ($("rubric > description html", xmlDoc).text().length > 0)
    descVal = $("rubric > description html", xmlDoc).text();
  else if ($("rubric > description text", xmlDoc).text())
    descVal = $("rubric > description val", xmlDoc).text();

  tinymce
    .get("rubricDesc")
    .setContent(withFormatting ? descVal : htmlDecode(descVal)); // retrieve and decode rubric description
  $("#rubricDesc").trigger("change");
  adjustRows(criteria.length);
  var levels = $("levels:eq(0) level", xmlDoc);
  const hasDescription = levels.length == 6;
  /** $( */
  /** `input[name="description"][value="${levels.length == 6 ? "1" : "0"}"]` */
  /** ).prop("checked", "checked"); */
  if (isText) $("#rubricTypeText").prop("checked", "checked");
  else $("#rubricTypePoints").prop("checked", "checked");
  switchCalculation();
  var maxMarks = [];
  var byWeight = $("#rubricCalcWeight").is(":checked"); // bool if calculation by weight (true) or by points (false)
  criteria.each(function (i, e) {
    var content = $(e);
    var thisRowCells = $("#rubricTable > tbody > tr:eq(" + i + ") > td");
    maxMarks[maxMarks.length] = parseFloat($("cell", e).attr("cell_value"));
    if (hasDescription) {
      $("textarea", thisRowCells.eq(0)).html(content.attr("name"));
    } else {
      var targetMCE = $(".editable", thisRowCells.eq(2)); // Retrieve target TinyMCE object

      tinymce.get(targetMCE.attr("id")).setContent(content.attr("name"));
      targetMCE.trigger("change");

      $("textarea", thisRowCells.eq(0)).html(`Criterion ${i + 1}`);
    }

    if (!byWeight)
      $("input.weights", thisRowCells.eq(1)).val(maxMarks[maxMarks.length - 1]); // By points not weight, so max can be added as the criteria points
    $("description", content).each(function (ind, el) {
      var targetMCE = $(
        ".editable",
        thisRowCells.eq((levels.length == 6 ? 0 : 2) + ind * 2 + 2)
      ); // Retrieve target TinyMCE object
      var newContent =
        $("text", el).text() == ""
          ? $("html", el).text()
          : $("text", el).text(); // Retrieve text descriptor
      if (newContent != "") {
        try {
          tinymce
            .get(targetMCE.attr("id"))
            .setContent(withFormatting ? newContent : htmlDecode(newContent));
          targetMCE.trigger("change");
        } catch (e) {
          console.error(e, targetMCE);
        }
      }
    });
  });
  if (byWeight) {
    var pointsTotal = localFloor(maxMarks.reduce((a, b) => a + b, 0)); // calculate total points to two decimal places
    $("#rubricGrade").val(pointsTotal); // if by weight, add total points to Points input
    for (var i = 0; i < maxMarks.length; i++) {
      $("#rubricTable tbody tr:eq(" + i + ") td input.weights").val(
        localFloor((maxMarks[i] / pointsTotal) * 100)
      ); // Add percentage as criteria weight
    }
  }
  $("#importFile").val("");
  recalculate();

  if (!byAutosave) {
    const btn = getReapplyButton();
    btn.hidden = false;
  }

  autosave();
}

function importZip(evt) {
  var $result = $("#result");
  $result.html("");
  if (evt.target.files != null && evt.target.files.length > 0) {
    JSZip.loadAsync(evt.target.files[0]).then(
      function (zip) {
        var bFailed = false;
        zip.forEach(function (relativePath, zipEntry) {
          if (
            !bFailed &&
            zipEntry.name != DEFLTFILE &&
            zipEntry.name != MANIFFILE
          ) {
            alert(
              "The chosen file does not appear to be a valid MyLO rubric file. To export a rubric from MyLO, go to your unit and select the cog in the top-right corner, and select Import/Export/Copy Components. Select Export Components then select the Start button, and follow the prompts to export just ONE rubric."
            );
            $("#importFile").val("");
            bFailed = true;
          }
          if (!bFailed && zipEntry.name == DEFLTFILE) {
            zipEntry.async("string").then(
              function success(content) {
                importXml(content);
              },
              function error(e) {
                $result.append(
                  $("<div>", {
                    class: "alert alert-danger",
                    text:
                      "Error extracting " +
                      evt.target.files[0].name +
                      ": " +
                      e.message,
                  })
                );
              }
            );
          }
        });
      },
      function (e) {
        $result.append(
          $("<div>", {
            class: "alert alert-danger",
            text:
              "Error reading " + evt.target.files[0].name + ": " + e.message,
          })
        );
      }
    );
  } else console.log(evt);
}

function saveToWordDocument() {
  let text_only = document.querySelector("#rubricTypeText").checked;
  /**
   * var bad_weights = document.querySelectorAll('input.weights.recalc[data-val="0"]').length;
   * if (bad_weights > 0 && !text_only) {
   *   alert(
   *     "There are " +
   *       bad_weights +
   *       " criterion rows that have a weight of 0. Please correct this before trying to download the rubric."
   *   );
   *   return;
   * }
   */

  var rTitle = $("#rubricTitle").val().trim();
  if (rTitle == "")
    return errorOut(
      "Please enter a title for your new Rubric.",
      "#rubricTitle"
    );

  let rubric = new DOMParser().parseFromString(getXml(true), "text/xml");
  let output = document.createElement("div");

  // output.style.position = 'absolute';
  // output.style.left = '-100000000000px';
  // document.body.appendChild(output);

  let name = rubric.querySelector("rubric").getAttribute("name");
  let details = document.createElement("div");
  details.innerHTML = `<h2>${name}</h2><small>${
    (
      rubric.querySelector("rubric > description text") ||
      rubric.querySelector("rubric > description html")
    ).textContent
  }</small>`;
  output.appendChild(details);

  let criteriaGroups = rubric.querySelectorAll(
    "criteria_groups criteria_group"
  );
  criteriaGroups.forEach((group) => {
    let table = document.createElement("table");
    let headerRow = document.createElement("tr");
    let criteriaHeader = document.createElement("td");
    criteriaHeader.innerHTML = "<strong>Criteria</strong>";
    headerRow.appendChild(criteriaHeader);
    group.querySelectorAll("level_set levels level").forEach((hdr) => {
      let cell = document.createElement("td");
      cell.innerHTML = `<strong>${hdr.getAttribute("name")}</strong>`;
      headerRow.appendChild(cell);
    });

    headerRow.querySelectorAll("td").forEach((td) => {
      Object.keys(cellStyles).forEach(
        (key) => (td.style[key] = cellStyles[key])
      );
    });

    headerRow.style.backgroundColor = rowColors.header;
    table.appendChild(headerRow);

    group.querySelectorAll("criteria criterion").forEach((crit, index) => {
      let row = document.createElement("tr");

      let critTitle = document.createElement("td");
      critTitle.innerText = crit.getAttribute("name");
      row.appendChild(critTitle);

      let points = crit.querySelector("cells cell").getAttribute("cell_value");
      if (points != null) {
        critTitle.innerHTML += `<p>&nbsp;</p><p>${points} point${
          points == "1" ? "" : "s"
        }</p>`;
      }

      crit.querySelectorAll("cells cell").forEach((cell) => {
        let content =
          cell.querySelector("description text") ||
          cell.querySelector("description html");
        let td = document.createElement("td");
        td.innerHTML = content.textContent;

        let desc = td.querySelector("div.description");
        if (desc) {
          desc.replaceWith(...desc.childNodes);
        }

        row.appendChild(td);
      });

      row.querySelectorAll("td").forEach((td) => {
        Object.keys(cellStyles).forEach(
          (key) => (td.style[key] = cellStyles[key])
        );
      });

      // Do an odd/even color pattern
      row.style.backgroundColor =
        index % 2 == 0 ? rowColors.odd : rowColors.even;

      table.appendChild(row);
    });

    Object.keys(tableStyles).forEach(
      (key) => (table.style[key] = tableStyles[key])
    );

    output.appendChild(table);

    let spacer = document.createElement("p");
    spacer.innerHTML = "&nbsp;";
    output.appendChild(spacer);
  });

  output
    .querySelectorAll("td")
    .forEach((td) =>
      td.setAttribute(
        "style",
        td.getAttribute("style") + "mso-border-alt:solid windowtext .5pt;"
      )
    );

  let blob = htmlDocx.asBlob(output.innerHTML, {
    orientation: "landscape",
    margins: {
      top: 720,
      bottom: 720,
      left: 720,
      right: 720,
    },
  });

  let filename = prompt("Rubric word document name", name + ".docx");
  if (!filename.endsWith(".docx")) {
    filename = filename + ".docx";
  }
  saveAs(blob, filename);
}

function htmlEncode(value) {
  return $("<div/>").text(value).html();
}

function htmlDecode(value) {
  return $("<div/>").html(value).text();
}

function errorOut(err, id) {
  alert(err);
  $(id).focus();
  return false;
}

function save_flags() {
  var flag = 0;
  if ($('input[name="rubricType"]:checked').val() == "0")
    flag += userstate_flags.RUBRIC_TYPE_CUSTOM_POINTS;
  if ($('input[name="rubricState"]:checked').val() == "0")
    flag += userstate_flags.RUBRIC_STATE_FINALISED;
  if ($('input[name="rubricCalc"]:checked').val() == "0")
    flag += userstate_flags.RUBRIC_POINT_CALC_BY_WEIGHT;
  if ($('input[name="description"]:checked').val() == "1")
    flag += userstate_flags.USE_DESCRIPTION_MANUAL;
  // if($('input[name="rubricDetails"]').is(':checked')) flag += userstate_flags.USE_DESCRIPTION;
  window.localStorage.setItem("userstate", flag);
}

function load_flags() {
  var flag = parseInt(window.localStorage.getItem("userstate"));
  // if(flag & userstate_flags.USE_DESCRIPTION) $('input[name="rubricDetails"]').attr('checked', 'checked');
  // if(flag & userstate_flags.ALWAYS_USE_DESCRIPTION) $('input[name="criteriaDetails"]').attr('checked', 'checked');
  $(
    'input[name="rubricType"]' +
      (flag & userstate_flags.RUBRIC_TYPE_CUSTOM_POINTS
        ? '[value="0"]'
        : ':not([value="0"])')
  ).attr("checked", "checked");
  $(
    'input[name="rubricState"]' +
      (flag & userstate_flags.RUBRIC_STATE_FINALISED
        ? '[value="0"]'
        : ':not([value="0"])')
  ).attr("checked", "checked");
  $(
    'input[name="rubricCalc"]' +
      (flag & userstate_flags.RUBRIC_POINT_CALC_BY_WEIGHT
        ? '[value="0"]'
        : ':not([value="0"])')
  ).attr("checked", "checked");

  var desc_val =
    flag & userstate_flags.USE_DESCRIPTION_AUTO
      ? 1
      : flag & userstate_flags.USE_DESCRIPTION_MANUAL
      ? 1
      : 0;
  $('input[name="description"][value="' + desc_val + '"]').attr(
    "checked",
    "checked"
  );
  switchCalculation();
}
