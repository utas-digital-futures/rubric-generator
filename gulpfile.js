const gulp = require('gulp');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify-es').default;
const sourcemap = require('gulp-sourcemaps');

// Provided by the GitLab CI/CD system.
//	Will currently be one of the following:
//		- master
//		- beta
const branch = process.env.CI_COMMIT_REF_NAME;
console.log('Building "%s"', `app${branch == 'beta' ? '.dev' : ''}.min.js`);

const minify = () => gulp.src('generator.js')
  .pipe(rename(`app${branch == 'beta' ? '.dev' : ''}.min.js`))
  .pipe(sourcemap.init())
  .pipe(uglify())
  .pipe(sourcemap.write(`./`))
  .pipe(gulp.dest(`./dist/`));

exports.minify = minify;