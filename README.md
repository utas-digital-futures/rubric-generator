# Rubric Generator
**Developed by @kloudmm and @connordeckers**

The [Rubric Generator](http://www.utas.edu.au/building-elearning/resources/mylo-rubric-generator/) is an interactive application that streamlines the process of Rubric creation for the D2L Brightspace platform.

## Features
- Import rubric directly from D2L MyLO, using [MyLO MATE](https://gitlab.com/SHS-UTAS/mylo-mate).
- Create rubric by pasting table from MS Word.
- Automatic point distribution.
- Capacity for rich-text `Description` column.
- Create rubric with preset Status (`Finalised` or `Under development`).
- Create rubrics that are `Text Only` or `Custom Points`.
- Import rubric from a D2L-created zip export.
- Save Rubric creation as `.zip` file, `.docx`, or to clipboard.

## Get involved
For feature requests or bug reports, you can [email us (MyLO.MATE@utas.edu.au)](mailto:MyLO.MATE@utas.edu.au), or get in touch on [Slack - #rubric-generator](https://building-elearning.slack.com/messages/CD87GT9M4).